from mus_deck import deck

def orderForDeal(player_list, dealer):
    
    ordered_list = []
    
    if (len(player_list) - dealer) > 1:
        for i in range(len(player_list) - dealer):
            ordered_list.append(player_list[dealer + i])
        for i in range(len(player_list) - (len(player_list) - dealer)):
            ordered_list.append(player_list[i])

    else:
        ordered_list.append(player_list[dealer])
        for i in range(len(player_list) - 1):
            ordered_list.append(player_list[i])
            
    return ordered_list

def deal(input_deck, player_list):
    
    for i in range(4):
        for i in range(4):
            player_list[i].add_to_hand(input_deck[0])
            input_deck.pop(0)
            
    return input_deck

def mus_deal(input_deck, player, sombat):
    
    for i in range(sombat):
        player.add_to_hand(input_deck[0])
        input_deck.pop(0)
        
def pair_search(input_hand):
    
    numpairs = 0
    hand = input_hand
    cardtype = []
    
    num0 = 0
    num1 = 0
    num2 = 0
    num3 = 0

    for i in range(len(hand) - 1):
        if input_hand[i] == input_hand[0]:
            num0 = num0 + 1
            numpairs = numpairs + 1
            
    for i in range(len(hand) - 1):
        if input_hand[i] == input_hand[1]:
            num1 = num1 + 1
            numpairs = numpairs + 1
            
    for i in range(len(hand) - 1):
        if input_hand[i] == input_hand[2]:
            num2 = num2 + 1
            numpairs = numpairs + 1
            
    for i in range(len(hand) - 1):
        if input_hand[i] == input_hand[3]:
            num3 = num3 + 1
            numpairs = numpairs + 1
     
    if numpairs > 0:        
        if num0 > 1:
            if num0 == 2:
                pairtype = "pair"
                cardtype.append(input_hand[0].value)
            elif num0 == 3:
                pairtype = "mediak"
                cardtype.append(input_hand[0].value)
            elif num0 == 4:
                pairtype = "dobleak"
                cardtype.append(input_hand[0].value)
            
        if num1 > 1:
            if num1 == 2:
                pairtype = "pair"
                cardtype.append(input_hand[1].value)
            elif num1 == 3:
                pairtype = "mediak"
                cardtype.append(input_hand[1].value)
            elif num1 == 4:
                pairtype = "dobleak"
                cardtype.append(input_hand[1].value)
            
        if num2 > 1:
            if num2 == 2:
                pairtype = "pair"
                cardtype.append(input_hand[2].value)
            elif num2 == 3:
                pairtype = "mediak"
                cardtype.append(input_hand[2].value)
            elif num2 == 4:
                pairtype = "dobleak"
                cardtype.append(input_hand[2].value)
            
        if num3 > 1:
            if num3 == 2:
                pairtype = "pair"
                cardtype.append(input_hand[3].value)
            elif num3 == 3:
                pairtype = "mediak"
                cardtype.append(input_hand[3].value)
            elif num3 == 4:
                pairtype = "dobleak"
                cardtype.append(input_hand[3].value)
    
    else:
        pairtype = "none"
        cardtype.append(0)           

    return pairtype, cardtype

def jokoa(input_hand):
    pundiak = 0
    for i in range(len(input_hand)):
        pundiak = pundiak + input_hand[i].pundiak
        
    if pundiak > 31:
        jokoa = True
        
    return jokoa, pundiak
        
def handiak_strength_analysis(input_hand):
    hand_values = [input_hand[0].value, input_hand[1].value, input_hand[2].value, input_hand[3].value]
    
    hand_max = max(hand_values)
    hand_min = min(hand_values)
    
    hand_values.remove(hand_max)
    hand_values.remove(hand_min)
    
    hand_2nd_max = max(hand_values)
    hand_2nd_min = min(hand_values)
    
    if hand_max == 10:
        hand_weight = 0.5
    elif hand_max == 9:
        hand_weight = 0.25
    elif hand_max == 8:
        hand_weight = 0.125
    elif hand_max == 7:
        hand_weight = 0.0625
    elif hand_max == 6:
        hand_weight = 0.03125
    elif hand_max == 5:
        hand_weight = 0.015625
    elif hand_max == 4:
        hand_weight = 0.0078125
    elif hand_max == 3:
        hand_weight = 0.00390625
    elif hand_max == 2:
        hand_weight = 0.0019531
    elif hand_max == 1:
        hand_weight = 0
        
    if hand_2nd_max == 10:
        hand_weight = hand_weight + 0.3
    elif hand_2nd_max == 9:
        hand_weight = hand_weight + 0.15
    elif hand_2nd_max == 8:
        hand_weight = hand_weight + 0.075
    elif hand_2nd_max == 7:
        hand_weight = hand_weight + 0.0325
    elif hand_2nd_max == 6:
        hand_weight = hand_weight + 0.01625
    elif hand_2nd_max == 5:
        hand_weight = hand_weight + 0.008125
    elif hand_2nd_max == 4:
        hand_weight = hand_weight + 0.0040625
    elif hand_2nd_max == 3:
        hand_weight = hand_weight + 0.00203125
    elif hand_2nd_max == 2:
        hand_weight = hand_weight + 0.00101562
    elif hand_2nd_max == 1:
        hand_weight = hand_weight + 0
    
    if hand_2nd_min == 10:
        hand_weight = hand_weight + 0.2
    elif hand_2nd_min == 9:
        hand_weight = hand_weight + 0.1
    elif hand_2nd_min == 8:
        hand_weight = hand_weight + 0.05
    elif hand_2nd_min == 7:
        hand_weight = hand_weight + 0.025
    elif hand_2nd_min == 6:
        hand_weight = hand_weight + 0.0125
    elif hand_2nd_min == 5:
        hand_weight = hand_weight + 0.000625
    elif hand_2nd_min == 4:
        hand_weight = hand_weight + 0.003125
    elif hand_2nd_min == 3:
        hand_weight = hand_weight + 0.0015625
    elif hand_2nd_min == 2: 
        hand_weight = hand_weight + 0.00078125
    elif hand_2nd_min == 1:
        hand_weight = hand_weight + 0
    
    return hand_weight
    
        