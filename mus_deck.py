# Class that contains cards in mus deck stored as strings
from mus_card import card
from random import randint

class deck:

    def __init__(deck):

        deck.ace_cups = card("Ace of Cups", 1, 1)
        deck.two_cups = card("Two of Cups", 2, 2)
        deck.three_cups = card("Three of Cups", 3, 3)
        deck.four_cups = card("Four of Cups", 4, 4)
        deck.five_cups = card("Five of Cups", 5, 5)
        deck.six_cups = card("Six of Cups", 6, 6)
        deck.seven_cups = card("Seven of Cups", 7, 7)
        deck.jack_cups = card("Jack of Cups", 8, 10)
        deck.knight_cups = card("Knight of Cups", 9, 10)
        deck.king_cups = card("King of Cups", 10, 10)
    
        deck.ace_coins = card("Ace of Coins", 1, 1)
        deck.two_coins = card("Two of Coins", 2, 2)
        deck.three_coins = card("Three of Coins", 3, 3)
        deck.four_coins = card("Four of Coins", 4, 4)
        deck.five_coins = card("Five of Coins", 5, 5)
        deck.six_coins = card("Six of Coins", 6, 6)
        deck.seven_coins = card("Seven of Coins", 7, 7)
        deck.jack_coins = card("Jack of Coins", 8, 10)
        deck.knight_coins = card("Knight of Coins", 9, 10)
        deck.king_coins = card("King of Coins", 10, 10)
    
        deck.ace_spades = card("Ace of Spades", 1, 1)
        deck.two_spades = card("Two of Spades", 2, 2)
        deck.three_spades = card("Three of Spades", 3, 3)
        deck.four_spades = card("Four of Spades", 4, 4)
        deck.five_spades = card("Five of Spades", 5, 5)
        deck.six_spades = card("Six of Spades", 6, 6)
        deck.seven_spades = card("Seven of Spades", 7, 7)
        deck.jack_spades = card("Jack of Spades", 8, 10)
        deck.knight_spades = card("Knight of Spades", 9, 10)
        deck.king_spades = card("King of Spades", 10, 10)
    
        deck.ace_clubs = card("Ace of Clubs", 1, 1)
        deck.two_clubs = card("Two of Clubs", 2, 2)
        deck.three_clubs = card("Three of Clubs", 3, 3)
        deck.four_clubs = card("Four of Clubs", 4, 4)
        deck.five_clubs = card("Five of Clubs", 5, 5)
        deck.six_clubs = card("Six of Clubs", 6, 6)
        deck.seven_clubs = card("Seven of Clubs", 7, 7)
        deck.jack_clubs = card("Jack of Clubs", 8, 10)
        deck.knight_clubs = card("Knight of Clubs", 9, 10)
        deck.king_clubs = card("King of Clubs", 10, 10)
    
        deck.deck_list = [deck.ace_cups, deck.two_cups, deck.three_cups,
                          deck.four_cups, deck.five_cups, deck.six_cups,
                          deck.seven_cups, deck.jack_cups, deck.knight_cups,
                          deck.king_cups, deck.ace_coins, deck.two_coins,
                          deck.three_coins, deck.four_coins, deck.five_coins,
                          deck.six_coins, deck.seven_coins, deck.jack_coins,
                          deck.knight_coins, deck.king_coins, deck.ace_spades,
                          deck.two_spades, deck.three_spades, deck.four_spades,
                          deck.five_spades, deck.six_spades, deck.seven_spades,
                          deck.jack_spades, deck.knight_spades, deck.king_spades,
                          deck.ace_clubs, deck.two_clubs, deck.three_clubs,
                          deck.four_clubs, deck.five_clubs, deck.six_clubs,
                          deck.seven_clubs, deck.jack_clubs, deck.knight_clubs,
                          deck.king_clubs]

    def shuffle(deck):

        deck.shuffle_list = deck.deck_list
        deck.shuffled_deck = []

        for i in range(40):
            rand = randint(0, (len(deck.shuffle_list) - 1))
            #print("deck.shuffle_list length: " + str(len(deck.shuffle_list)))
            #print("Rand: " + str(rand))
            #print("i: " + str(i))
            deck.shuffled_deck.append(deck.shuffle_list[rand])
            deck.shuffle_list.pop(rand)

        return deck.shuffled_deck
