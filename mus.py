from mus_deck import deck
from mus_player import player
from mus_card import card
import mus_functions

mus_deck = deck()

Ambroise = player("Ambroise", 0)
Sebastien = player("Sebastein", 1)
William = player("William", 2)
Eneko = player("Eneko", 3)

dealer = Ambroise.position

player_list = [Ambroise, Sebastien, William, Eneko]

inGame = True
inMancha = True
inHand = True
Mus = True

while inGame:

    while inMancha:
    
        while inHand:
            # Hand start
            shuffled_deck = deck.shuffle(mus_deck)
            ordered_player_list = mus_functions.orderForDeal(player_list, dealer)
            dealt_deck = mus_functions.deal(shuffled_deck, ordered_player_list)
            
            Ambroise.print_hand()
            
            esku = ordered_player_list[1]
            
            if ordered_player_list[1].name == "Ambroise":
                if (esku.musVote() and ordered_player_list[3].musVoteBot) == False:
                    esku.handiakBet()
                    if esku.handiakBet() == "paso":
                    
            else:
                if esku.musVoteBot() == False:
                    esku.handiakBetBot()
                
            # Players examine cards, determine Mus
            while Mus:
                
                if Ambroise.musVote() and William.musVoteBot():
                    if Sebastien.musVoteBot() and Eneko.musVoteBot():
                        Mus = True
                else:
                    Mus = False
                    
                if Mus == True:               
                    Ambroise.sombat = Ambroise.mus()
                    William.sombat = William.botMus()
                    Sebastien.sombat = Sebastien.botMus()
                    Eneko.sombat = Eneko.botMus()
                
                    for i in range(len(player_list)):
                        ordered_player_list[i].sombat = ordered_player_list[i].botMus()
                    
                    for i in range(len(player_list)):
                        mus_functions.mus_deal(dealt_deck, ordered_player_list[i], ordered_player_list[i].sombat)

            # handiak   

            dealer = esku


            Ambroise.print_hand()
            Sebastien.print_hand()
            William.print_hand()
            Eneko.print_hand()

#for i in range(len(player_list)):
#    ordered_player_list[i].sombat = ordered_player_list[i].mus()

#for i in range(len(player_list)):
#    mus_functions.mus_deal(dealt_deck, ordered_player_list[i], ordered_player_list[i].sombat)

#print("\n\n")
#Ambroise.print_hand()
#Sebastien.print_hand()
#William.print_hand()
#Eneko.print_hand()