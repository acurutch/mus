from mus_card import card
import mus_functions

class player:

    def __init__(player, name, position):
        player.name = name
        player.hand = []
        player.position = position

    def add_to_hand(player, card):
        player.hand.append(card)

    def print_hand(player):
        print(player.name + "'s hand")
        for i in range(len(player.hand)):
            card.printName(player.hand[i])

    def mus(player):
        print("How many cards would you like to discard?")
        sombat = int(input())
        print("Which cards would you like to discard?")
        
        cardnum_list = []
        
        for i in range(sombat):
            cardnum_list.append(int(input()))
            
        cardnum_list.sort()
        cardnum_list.reverse()
        
        print(cardnum_list)
            
        for i in range(sombat):
            player.hand.pop(cardnum_list[i])
            
        return sombat
    
    def botMus(player):
        
        return sombat

    def hand_analysis(player):
        
        pairtype, pair_cardtype = mus_functions.pair_search(player.hand)
        
        jokoa, pundiak = mus_functions.jokoa(player.hand)
        
        # handiak strength
        handiak_strength = mus_functions.handiak_strength_analysis(player.hand)
        # tthipiak strength
        tthipiak_strength = mus_functions.tthipiak_strength_analysis(player.hand)
        # pareak strength
        pareak_strength = mus_functions.pareak_strength_analysis(pairtype, pair_cardtype)
        
        # jokoa / pundiak strength
        if jokoa:
            jokoa_strength = mus_functions.jokoa_strength_analysis(pundiak)
            player.jokoa_strength = jokoa_strength
        else:
            pundiak_strength = mus_functions.pundiak_strength_analysis(pundiak)
            player.pundiak_strengh = pundiak_strength
        
        player.handiak_strength = handiak_strength
        player.tthipiak_strength = tthipiak_strength
        player.pareak_strength = pareak_strength
        player.jokoa = jokoa
        
    def musVote(player):
        Mus = False
        return Mus
    
    def musVoteBot(player):
        Mus = False
        return Mus
        